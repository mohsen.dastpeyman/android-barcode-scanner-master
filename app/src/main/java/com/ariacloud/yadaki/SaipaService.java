package com.ariacloud.yadaki;

import java.util.List;

import retrofit.Call;
import retrofit.http.GET;
import retrofit.http.Path;
import retrofit.http.Query;

/**
 * Created by mohsen on 6/24/17.
 */

public interface SaipaService {
    @GET("services/mobile/partPrice.php?password=GrntYadak12")
    Call<List<saipa_model>> getSaipaList(@Query("make") String idBrand, @Query("partDesc") String namePice);
}
