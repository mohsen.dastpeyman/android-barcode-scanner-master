package com.ariacloud.yadaki;

import android.*;
import android.Manifest;
import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.dx.dxloadingbutton.lib.LoadingButton;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.jar.*;

import ir.adad.client.Adad;
import ir.adad.client.InterstitialAdListener;
import pl.droidsonroids.gif.GifImageView;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;


public class HomeActivity extends AppCompatActivity {
    private String m, codeContent = null;
    private TextView priceTxt, ncarTxt, npieceTxt, cpieceTxt, comanyTxt;
    private EditText contentTxt;
    public static final int CONNECTION_TIMEOUT = 10000;
    public static final int READ_TIMEOUT = 15000;
    InterstitialAd mInterstitialAd;
    private Button btn_scan,bsearch;
    private AdView mAdView;
    LoadingButton send;
    GifImageView gib;
    int count;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("font/Samim.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
        super.onCreate(savedInstanceState);
//        Adad.initialize(getApplicationContext());


        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId("ca-app-pub-7250447532514086/9539495054");


        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                requestNewInterstitial();
//                Toast.makeText(getApplication(), "لود نشده", Toast.LENGTH_LONG).show();

            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
              //  Toast.makeText(getApplicationContext(), String.valueOf(errorCode), Toast.LENGTH_SHORT).show();
                requestNewInterstitial();
            }

            @Override
            public void onAdLoaded() {
//                Toast.makeText(getApplicationContext(), "Ad is loaded!", Toast.LENGTH_SHORT).show();
            }
        });
        requestNewInterstitial();

        setContentView(R.layout.activity_home);
        mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder()
                .build();
        mAdView.loadAd(adRequest);

//        mAdView.setVisibility(View.VISIBLE);
        getWindow().getDecorView().
                setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
//adad initialize

// --------------------
        contentTxt = (EditText) findViewById(R.id.scan_content);
 /*       contentTxt.setOnFocusChangeListener(new View.OnFocusChangeListener() {

            @Override
            public void onFocusChange(View v, boolean hasFocus) {

                mAdView.setVisibility(hasFocus ? View.INVISIBLE : View.VISIBLE);

            }
        });*/
        priceTxt = (TextView) findViewById(R.id.price);
        ncarTxt = (TextView) findViewById(R.id.ncar);
        npieceTxt = (TextView) findViewById(R.id.npiece);
        cpieceTxt = (TextView) findViewById(R.id.cpiece);
        comanyTxt = (TextView) findViewById(R.id.company);

        btn_scan = (Button) findViewById(R.id.btn_scan_now);
        btn_scan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                scanNow(v);
            }
        });
        send = (LoadingButton) findViewById(R.id.send);

        gib = (GifImageView) findViewById(R.id.tabligh);
        gib.setVisibility(View.VISIBLE);
        gib = (GifImageView) findViewById(R.id.tabligh);
        gib.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri uri = Uri.parse("https://cafebazaar.ir/app/com.ariacloud.shahrbin/?l=fa");
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
            }
        });

        bsearch = (Button) findViewById(R.id.search);
        bsearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Context context = v.getContext();
                Intent intent = new Intent(context, Search.class);


                context.startActivity(intent);
//                    overridePendingTransition(R.anim.push_in_from_down, R.anim.push_out_to_down);
            }
        });

        mAdView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                // Code to be executed when an ad finishes loading.
                //Log.i("Ads", "onAdLoaded");
                gib.setVisibility(View.INVISIBLE);
                mAdView.setVisibility(View.VISIBLE);

            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                // Code to be executed when an ad request fails.
                Log.i("Ads", "onAdFailedToLoad");
                gib.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAdOpened() {
                // Code to be executed when an ad opens an overlay that
                // covers the screen.
                Log.i("Ads", "onAdOpened");
                gib.setVisibility(View.INVISIBLE);
                mAdView.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAdLeftApplication() {
                // Code to be executed when the user has left the app.
                Log.i("Ads", "onAdLeftApplication");
            }

            @Override
            public void onAdClosed() {
                // Code to be executed when when the user is about to return
                // to the app after tapping on an ad.
                Log.i("Ads", "onAdClosed");
                gib.setVisibility(View.VISIBLE);
                mAdView.setVisibility(View.INVISIBLE);
            }
        });

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.about:
                Intent Iabout = new Intent(HomeActivity.this, About.class);
                startActivity(Iabout);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * event handler for scan button
     *
     * @param view view of the activity
     */
    public void scanNow(View view) {

        if (ContextCompat.checkSelfPermission(HomeActivity.this,
                Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(HomeActivity.this,
                    new String[]{Manifest.permission.CAMERA},
                    1);
        } else {
            IntentIntegrator integrator = new IntentIntegrator(this);
            integrator.setDesiredBarcodeFormats(IntentIntegrator.ONE_D_CODE_TYPES);
            integrator.setPrompt(this.getString(R.string.scan_bar_code));
            integrator.setResultDisplayDuration(0);
            integrator.setWide();  // Wide scanning rectangle, may work better for 1D barcodes
            integrator.setCameraId(0);  // Use a specific camera of the device
            integrator.initiateScan();
        }


//    new  AsyncPostBarcode().execute();
    }

    public void send_barcode(View view) throws InterruptedException {
//        Adad.prepareInterstitialAd(mAdListener);


        visaible_Textview();
        hideSoftKeyboard();
        m = contentTxt.getText().toString();
        if (!m.matches("")) {
//            if (m.matches("[0-9.? ]*")) {
            if (send.getCurrentState() == 0) {
                send.startLoading();
                count=0;
                new AsyncPostBarcode().execute();
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        if (count == 0) {
                            Toast.makeText(HomeActivity.this, getApplication().getResources().getString(R.string.connectionError), Toast.LENGTH_LONG).show();
                        }
                        else if(count == 2){
                            Toast.makeText(HomeActivity.this, getApplication().getResources().getString(R.string.barcodeError), Toast.LENGTH_LONG).show();
                        }

                        send.cancelLoading();

                    }
                }, 10000);

//            } else {
//                Toast.makeText(getApplication(), getApplication().getResources().getString(R.string.baecodeInputError), Toast.LENGTH_LONG).show();
//            }
            }
        }

    }

    private void requestNewInterstitial() {
        if (!mInterstitialAd.isLoading() && !mInterstitialAd.isLoaded()) {
            AdRequest adRequest = new AdRequest.Builder().build();
            mInterstitialAd.loadAd(adRequest);
        }
    }

    /**
     * function handle scan result
     *
     * @param requestCode scanned code
     * @param resultCode  result of scanned code
     * @param intent      intent
     */
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        //retrieve scan result
        IntentResult scanningResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);

        if (scanningResult.getContents() != null) {
            //we have a result
            codeContent = scanningResult.getContents();

            // display it on screen
            contentTxt.setText(codeContent);
//            new  AsyncPostBarcode().execute();

        }
    }


    private class AsyncPostBarcode extends AsyncTask<String, String, String> {
        //        ProgressDialog pdLoading = new ProgressDialog(getApplication());
        ProgressDialog pdLoadingPostEvent;

        HttpURLConnection client;
        URL url = null;
        private Exception exceptionToBeThrown;


        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            //this method will be running on UI thread
/*            pdLoadingPostEvent = new ProgressDialog(HomeActivity.this);
            pdLoadingPostEvent.setMessage("\tدر حال انتظار ");

            pdLoadingPostEvent.show();
            pdLoadingPostEvent.setCancelable(false);*/

        }


        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {

            try {
                InputStream input;
                url = new URL("http://shahrbyn.ir/isaco/");
                //52.10.230.243
                client = (HttpURLConnection) url.openConnection();
//                client.setDoOutput(true);
//                client.setDoInput(true);
                client.setUseCaches(false);
                client.setReadTimeout(READ_TIMEOUT);
                client.setConnectTimeout(CONNECTION_TIMEOUT);

                client.setRequestMethod("POST");
                String barcode = "code=" + URLEncoder.encode(m, "UTF-8");

//----------------
                try (DataOutputStream wr = new DataOutputStream(client.getOutputStream())) {
                    wr.writeBytes(barcode);
                }
                int responseCode = client.getResponseCode();
//--------------------------adad show---------------------
//                Adad.showInterstitialAd(HomeActivity.this);


//-----------------------------------------------------------
                if (responseCode == client.HTTP_OK) {
                    input = client.getInputStream();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(input));
                    StringBuilder result = new StringBuilder();
                    String line;

                    while ((line = reader.readLine()) != null) {
                        result.append(line);
                        result.append('\r');
                    }
                    reader.close();
                    count=1;
                    return result.toString();


                } else if (responseCode == 402) {
                    count=2;
                    return ("Input_Error");
                } else if (responseCode == 400) {
                    count=2;
                    return ("unsuccessful");
                } else {
                   count=2;
                    return ("unsuccessful");
                }
            } catch (IOException e1) {
                Log.e("ERROR", "ERROR IN CODE: " + e1.toString());

//                e1.printStackTrace();
//            Toast.makeText(HomeActivity.this, "مشکل در اتصال به اینترنت", Toast.LENGTH_LONG).show();
                e1.printStackTrace();
                exceptionToBeThrown = e1;
                return ("NoInternet");
//
//                pdLoading.cancel();
            }

            //POST section
            finally {

                if (client != null) {
                    client.disconnect();
//                    pdLoadingPostEvent.cancel();


                }


            }


        }

        protected void onPostExecute(String unused) {
            try {
                if (mInterstitialAd.isLoaded() && mInterstitialAd != null) {

                    mInterstitialAd.show();
                } else {
                    requestNewInterstitial();


                }
                if (unused == "NoInternet" && exceptionToBeThrown != null) {
//                    Toast.makeText(HomeActivity.this, getApplication().getResources().getString(R.string.connectionError), Toast.LENGTH_LONG).show();
                    //send.cancelLoading();

                } else if (unused != "unsuccessful" && exceptionToBeThrown == null) {

                    JSONArray jArray = new JSONArray(unused);
                    // Extract data from json and store into ArrayList as class objects
                    for (int i = 0; i < 1; i++) {
                        JSONObject json_data = jArray.getJSONObject(i);

                        priceTxt.setText("قیمت : " + json_data.getString("قیمت"));
                        priceTxt.setVisibility(View.VISIBLE);
                        npieceTxt.setText("نام قطعه : " + json_data.getString("نام قطعه"));
                        npieceTxt.setVisibility(View.VISIBLE);
                        ncarTxt.setText("نام خودرو : " + json_data.getString("نام خودرو"));
                        ncarTxt.setVisibility(View.VISIBLE);
                        cpieceTxt.setText("کد قطعه : " + json_data.getString("کد قطعه"));
                        cpieceTxt.setVisibility(View.VISIBLE);
                        comanyTxt.setText("شرکت : " + json_data.getString("شرکت"));
                        comanyTxt.setVisibility(View.VISIBLE);
                    }
                    send.cancelLoading();
                }
                // Setup and Handover data to recyclerview
                else {
                    count=2;
//                    Toast.makeText(HomeActivity.this, getApplication().getResources().getString(R.string.barcodeError), Toast.LENGTH_LONG).show();
                   // send.cancelLoading();
                }

            } catch (JSONException e) {
                count=2;
//                Toast.makeText(HomeActivity.this, getApplication().getResources().getString(R.string.barcodeError), Toast.LENGTH_LONG).show();
                //send.cancelLoading();
            }
        }


    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    public void hideSoftKeyboard() {
        if (getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }

    public void visaible_Textview() {
        priceTxt.setVisibility(View.INVISIBLE);
        ncarTxt.setVisibility(View.INVISIBLE);
        npieceTxt.setVisibility(View.INVISIBLE);
        cpieceTxt.setVisibility(View.INVISIBLE);
        comanyTxt.setVisibility(View.INVISIBLE);

    }

    private InterstitialAdListener mAdListener = new InterstitialAdListener() {
        @Override
        public void onAdLoaded() {
//            Toast.makeText(getApplicationContext(), "Interstitial Ad loaded", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onAdFailedToLoad() {
//            Toast.makeText(getApplicationContext(), "Interstitial Ad failed to load", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onMessageReceive(JSONObject message) {
//            Toast.makeText(getApplicationContext(), "Interstitial ", Toast.LENGTH_SHORT).show();

        }

        @Override
        public void onRemoveAdsRequested() {
//            Toast.makeText(getApplicationContext(), "User requested to remove interstitial ads from app", Toast.LENGTH_SHORT).show();
            //Move your user to shopping center of your app
        }

        @Override
        public void onInterstitialAdDisplayed() {
//            Toast.makeText(getApplicationContext(), "display ", Toast.LENGTH_SHORT).show();

        }

        @Override
        public void onInterstitialClosed() {
//            Toast.makeText(getApplicationContext(),"Interstitial Ad closed", Toast.LENGTH_SHORT).show();
        }

    };

    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(HomeActivity.this)
//                .setTitle("خروج")
                .setMessage("آیا می خواهید از برنامه خارج شوید ؟")
                .setPositiveButton("بله", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }

                })
                .setNegativeButton("خیر", null)
                .show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {

                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    btn_scan.callOnClick();
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Toast.makeText(HomeActivity.this, "اجازه دسترسی به دوربین داده نشد", Toast.LENGTH_SHORT).show();
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }
/*    @Override
    public void onPause() {
        if (mAdView != null) {
            mAdView.pause();
        }
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mAdView != null) {
            mAdView.resume();
        }
    }

    @Override
    public void onDestroy() {
        if (mAdView != null) {
            mAdView.destroy();
        }
        super.onDestroy();
    }*/

}
