package com.ariacloud.yadaki;

import com.google.gson.annotations.SerializedName;

/**
 * Created by mohsen on 6/29/17.
 */

public class isaco_model {
    @SerializedName("نام قطعه")
    public String partDesc;
    @SerializedName("مارک")
    public String mark;
    @SerializedName("قیمت")
    public String price;

    public void setpartDesc(String partDesc) {
        this.partDesc = partDesc;
    }

    public String getpartDesc() {
        return partDesc;
    }

    public void setmark(String mark) {
        this.mark = mark;
    }

    public String getmark() {
        return mark;
    }
    public void setprice(String price) {
        this.price = price;
    }

    public String getPrice() {
        return price;
    }
}
