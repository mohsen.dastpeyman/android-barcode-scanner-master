package com.ariacloud.yadaki;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.webkit.WebView;
import android.webkit.WebViewClient;

/**
 * Created by mohsen on 4/16/17.
 */

public class MyWebViewClient extends WebViewClient {

    public MyWebViewClient(About about) {
        super();
        //start anything you need to
    }

    public boolean shouldOverrideUrlLoading(WebView view, String url) {
        if (url != null && url.startsWith("https://")) {
            view.getContext().startActivity(
                    new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
            return true;
        } else {
            return false;
        }
    }

    public void onPageStarted(WebView view, String url, Bitmap favicon) {
        //Do something to the urls, views, etc.
    }
}