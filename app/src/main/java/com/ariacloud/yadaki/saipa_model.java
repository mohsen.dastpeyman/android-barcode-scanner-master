package com.ariacloud.yadaki;

import com.google.gson.annotations.SerializedName;

/**
 * Created by mohsen on 6/24/17.
 */

public class saipa_model {
    @SerializedName("partDesc")
    public String partDesc;
    @SerializedName("mark")
    public String mark;
    @SerializedName("price")
    public String price;

    public void setpartDesc(String partDesc) {
        this.partDesc = partDesc;
    }

    public String getpartDesc() {
        return partDesc;
    }

    public void setmark(String mark) {
        this.mark = mark;
    }

    public String getmark() {
        return mark;
    }
    public void setprice(String price) {
        this.price = price;
    }

    public String getPrice() {
        return price;
    }
}
