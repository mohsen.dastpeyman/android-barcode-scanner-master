package com.ariacloud.yadaki;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.MediaController;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.dx.dxloadingbutton.lib.LoadingButton;
import com.github.aakira.expandablelayout.ExpandableRelativeLayout;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;

import java.util.ArrayList;
import java.util.List;

import pl.droidsonroids.gif.GifImageButton;
import pl.droidsonroids.gif.GifImageView;
import retrofit.Call;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by mohsen on 6/22/17.
 */

public class Search extends AppCompatActivity {

    private Spinner spBrand;
    private RadioGroup rgMaker;
    private RadioButton rbSaipa, rbIsaco, rbModiran;
    private String[] saipaArray;
    private String[] isacoArray;
    private String[] saipaArray_id;
    private String[] modiranArray;
    private RecyclerView recyclerView;
    private AdView mAdView;
    private ArrayList<String> data;
    private int count;
    String brandTitle, temp;
    Integer brandNumber = 0;
    EditText etName;
    //    Button bSearch;
    String siapaURL, isacoURL, modiranURL = null;
    ExpandableRelativeLayout expandableLayout1;
    GifImageView gib;
    LoadingButton bSearch;
    InterstitialAd mInterstitialAd;

    public void onCreate(Bundle savedInstanceState) {
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("font/Samim-FD.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
        super.onCreate(savedInstanceState);

        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId("ca-app-pub-7250447532514086/9539495054");


        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                requestNewInterstitial();
//                Toast.makeText(getApplication(), "لود نشده", Toast.LENGTH_LONG).show();

            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                //  Toast.makeText(getApplicationContext(), String.valueOf(errorCode), Toast.LENGTH_SHORT).show();
                requestNewInterstitial();
            }

            @Override
            public void onAdLoaded() {
//                Toast.makeText(getApplicationContext(), "Ad is loaded!", Toast.LENGTH_SHORT).show();
            }
        });
        requestNewInterstitial();



        setContentView(R.layout.search);
        getWindow().getDecorView().
                setLayoutDirection(View.LAYOUT_DIRECTION_RTL);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_forward_white_24dp);

        Resources resources = getApplication().getResources();
        saipaArray = resources.getStringArray(R.array.saipa_array);
        isacoArray = resources.getStringArray(R.array.isaco_array);
        modiranArray = resources.getStringArray(R.array.modiran_array);


        gib = (GifImageView) findViewById(R.id.tabligh);
        gib.setVisibility(View.VISIBLE);
        gib = (GifImageView) findViewById(R.id.tabligh);


        gib.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri uri = Uri.parse("https://cafebazaar.ir/app/com.ariacloud.shahrbin/?l=fa");
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
            }
        });

        saipaArray_id = resources.getStringArray(R.array.saipa_array_id);
        spBrand = (Spinner) findViewById(R.id.spbrand);
        etName = (EditText) findViewById(R.id.pname);
        rgMaker = (RadioGroup) findViewById(R.id.radiomaker);

        rbSaipa = (RadioButton) findViewById(R.id.saipa);
        rbIsaco = (RadioButton) findViewById(R.id.isaco);
        rbModiran = (RadioButton) findViewById(R.id.modiran);
        Button expandableButton1 = (Button) findViewById(R.id.expandableButton1);


        recyclerView = (RecyclerView) findViewById(R.id.list);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);

        final ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(
                getBaseContext(), R.array.isaco_array, R.layout.spinner_item);
        adapter.setDropDownViewResource(R.layout.spinner_item);
        spBrand.setAdapter(adapter);
        bSearch = (LoadingButton) findViewById(R.id.search);
        mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder()
                .build();
        mAdView.loadAd(adRequest);

        bSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (etName.getText().toString().trim().length() >= 3) {
                    if (bSearch.getCurrentState() == 0) {
                        count = 0;
                        bSearch.startLoading();
                        hideSoftKeyboard();
                            if (mInterstitialAd.isLoaded() && mInterstitialAd != null) {

                                mInterstitialAd.show();
                            } else {
                                requestNewInterstitial();


                            }
//-----------------------------------------------SAIPA-------------------------------------------------------------
                        if (rbSaipa.isChecked()) {
                            temp = etName.getText().toString();
                            String s = saipaArray_id[brandNumber];
                            siapaURL = "Http://shahrbyn.ir";

                            String brand = "saipa";
                            final Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {

                                    if (count == 0) {
                                        Toast.makeText(Search.this, getApplication().getResources().getString(R.string.connectionError), Toast.LENGTH_LONG).show();
                                    }

                                    bSearch.cancelLoading();

                                }
                            }, 5000);
                            Retrofit retrofit = new Retrofit.Builder()
                                    .baseUrl(siapaURL)
                                    .addConverterFactory(GsonConverterFactory.create())
                                    .build();
                            IsacoService isacoservice = retrofit.create(IsacoService.class);
                            Call<List<isaco_model>> call = isacoservice.postIsaco(new Data(s, temp, brand));
                            Callback<List<isaco_model>> callback = new Callback<List<isaco_model>>() {

                                @Override
                                public void onResponse(Response<List<isaco_model>> response, Retrofit retrofit) {
                                    count++;
                                    if (response.isSuccess()) {
                                        ArrayList<isaco_model> data = new ArrayList<>();
                                        data.addAll(response.body());

                                        //data = new ArrayList<>(Arrays.asList(jsonResponse.getAndroid()));
                                        if (response.body().size() <= 1) {
                                            bSearch.cancelLoading();
                                            Toast.makeText(getApplicationContext(), "هیچ موردی یافت نشد", Toast.LENGTH_SHORT).show();
                                        } else {
                                            //lb.loadingSuccessful();
                                            bSearch.cancelLoading();
                                            recyclerView.setAdapter(new RecycleViewIsaco(data));
                                        }
/*                           for( saipa_model saipa: response.body()){
                                Log.d("saipa","name: "+saipa.getpartDesc()+" gheymat: "+saipa.getPrice());
                            }*/
                                    } else {
                                        System.out.println(response.errorBody());
                                        bSearch.cancelLoading();
                                    }


                                }


                                @Override
                                public void onFailure(Throwable t) {
//                                        Toast.makeText(getApplicationContext(), "مشکل در اتصال اینترنت", Toast.LENGTH_SHORT).show();
                                    bSearch.cancelLoading();
                                }
                            };
                            call.enqueue(callback);
                        }
//-----------------------------------------------ISACO-------------------------------------------------------------
                        else if (rbIsaco.isChecked()) {
                            isacoURL = "Http://shahrbyn.ir";
                            temp = etName.getText().toString();
                            String s = isacoArray[brandNumber];
                            String brand = "isaco";
                            final Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {

                                    if (count == 0) {
                                        Toast.makeText(Search.this, getApplication().getResources().getString(R.string.connectionError), Toast.LENGTH_LONG).show();
                                    }

                                    bSearch.cancelLoading();

                                }
                            }, 15000);
                            Retrofit retrofit = new Retrofit.Builder()
                                    .baseUrl(isacoURL)
                                    .addConverterFactory(GsonConverterFactory.create())
                                    .build();
                            IsacoService isacoservice = retrofit.create(IsacoService.class);
                            Call<List<isaco_model>> call = isacoservice.postIsaco(new Data(s, temp, brand));
                            Callback<List<isaco_model>> callback = new Callback<List<isaco_model>>() {

                                @Override
                                public void onResponse(Response<List<isaco_model>> response, Retrofit retrofit) {
                                    count++;
                                    if (response.isSuccess()) {
                                        ArrayList<isaco_model> data = new ArrayList<>();
                                        data.addAll(response.body());
                                        if (response.body().size() <= 1) {
                                            bSearch.cancelLoading();
                                            Toast.makeText(getApplicationContext(), "هیچ موردی یافت نشد", Toast.LENGTH_SHORT).show();
                                        } else {

                                            bSearch.cancelLoading();
                                            recyclerView.setAdapter(new RecycleViewIsaco(data));
                                        }

                                    } else {
                                        System.out.println(response.errorBody());
                                        bSearch.cancelLoading();
                                    }


                                }

                                @Override
                                public void onFailure(Throwable t) {
//                                        Toast.makeText(getApplicationContext(), "مشکل در اتصال اینترنت", Toast.LENGTH_SHORT).show();
                                    bSearch.cancelLoading();
                                }
                            };
                            call.enqueue(callback);

                        }
//-----------------------------------------------MODIRAN KHODRO---------------------------------------------------------
                        else if (rbModiran.isChecked()) {
                            modiranURL = "Http://shahrbyn.ir";
                            temp = etName.getText().toString();
                            String s = modiranArray[brandNumber];
                            String brand = "mvm";

                            final Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {

                                    if (count == 0) {
                                        Toast.makeText(Search.this, getApplication().getResources().getString(R.string.connectionError), Toast.LENGTH_LONG).show();
                                    }

                                    bSearch.cancelLoading();

                                }
                            }, 5000);
                            Retrofit retrofit = new Retrofit.Builder()
                                    .baseUrl(modiranURL)
                                    .addConverterFactory(GsonConverterFactory.create())
                                    .build();

                            IsacoService isacoservice = retrofit.create(IsacoService.class);
                            Call<List<isaco_model>> call = isacoservice.postIsaco(new Data(s, temp, brand));
                            Callback<List<isaco_model>> callback = new Callback<List<isaco_model>>() {

                                @Override
                                public void onResponse(Response<List<isaco_model>> response, Retrofit retrofit) {
                                    count++;
                                    if (response.isSuccess()) {

                                        ArrayList<isaco_model> data = new ArrayList<>();
                                        data.addAll(response.body());
                                        if (response.body().size() <= 1) {
                                            bSearch.cancelLoading();
                                            Toast.makeText(getApplicationContext(), "هیچ موردی یافت نشد", Toast.LENGTH_SHORT).show();
                                        } else {
                                            bSearch.cancelLoading();
                                            recyclerView.setAdapter(new RecycleViewIsaco(data));
                                        }

                                    } else {
                                        bSearch.cancelLoading();
                                        System.out.println(response.errorBody());
                                    }


                                }


                                @Override
                                public void onFailure(Throwable t) {
                                    //   Toast.makeText(getApplicationContext(), "اشکال در اتصال اینترنت",Toast.LENGTH_SHORT).show();
                                    bSearch.cancelLoading();
                                }
                            };
                            call.enqueue(callback);
//                                               bSearch.setClickable(true);
//
                        }
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "حداقل 3 حرف باید باشد", Toast.LENGTH_SHORT).show();

                }
            }


        });


        spBrand.setSelection(brandNumber);
        spBrand.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapter, View v,
                                       int position, long id) {
                // On selecting a spinner item
                brandTitle = adapter.getItemAtPosition(position).toString();
                brandNumber = position;
                // Showing selected spinner item
                //Toast.makeText(getApplicationContext(), "انتخاب شهر : " + isacoArray[brandNumber], Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub

            }
        });

        rgMaker.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // TODO Auto-generated method stub
                if (rbSaipa.isChecked()) {
                    ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(
                            getBaseContext(), R.array.saipa_array, R.layout.spinner_item);
                    adapter.setDropDownViewResource(R.layout.spinner_item);
                    spBrand.setAdapter(adapter);

                } else if (rbIsaco.isChecked()) {
                    ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(
                            getBaseContext(), R.array.isaco_array, R.layout.spinner_item);
                    adapter.setDropDownViewResource(R.layout.spinner_item);
                    spBrand.setAdapter(adapter);
                } else if (rbModiran.isChecked()) {
                    ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(
                            getBaseContext(), R.array.modiran_array, R.layout.spinner_item);
                    adapter.setDropDownViewResource(R.layout.spinner_item);
                    spBrand.setAdapter(adapter);
                }
            }
        });

        mAdView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                // Code to be executed when an ad finishes loading.
                //Log.i("Ads", "onAdLoaded");
                gib.setVisibility(View.INVISIBLE);
                mAdView.setVisibility(View.VISIBLE);

            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                // Code to be executed when an ad request fails.
                Log.i("Ads", "onAdFailedToLoad");
                gib.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAdOpened() {
                // Code to be executed when an ad opens an overlay that
                // covers the screen.
                Log.i("Ads", "onAdOpened");
                gib.setVisibility(View.INVISIBLE);
                mAdView.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAdLeftApplication() {
                // Code to be executed when the user has left the app.
                Log.i("Ads", "onAdLeftApplication");
            }

            @Override
            public void onAdClosed() {
                // Code to be executed when when the user is about to return
                // to the app after tapping on an ad.
                Log.i("Ads", "onAdClosed");
                gib.setVisibility(View.VISIBLE);
                mAdView.setVisibility(View.INVISIBLE);
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:

                finish();


                break;
        }
        return true;
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    public void expandableButton1(View view) {
        expandableLayout1 = (ExpandableRelativeLayout) findViewById(R.id.expandableLayout1);
        expandableLayout1.toggle(); // toggle expand and collapse
    }

    public void hideSoftKeyboard() {
        if (getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }
    private void requestNewInterstitial() {
        if (!mInterstitialAd.isLoading() && !mInterstitialAd.isLoaded()) {
            AdRequest adRequest = new AdRequest.Builder().build();
            mInterstitialAd.loadAd(adRequest);
        }
    }
}

