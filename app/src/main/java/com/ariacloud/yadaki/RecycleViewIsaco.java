package com.ariacloud.yadaki;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.ArrayList;

/**
 * Created by mohsen on 6/29/17.
 */

public class RecycleViewIsaco extends RecyclerView.Adapter<RecycleViewIsaco.ViewHolder>{

    private ArrayList<isaco_model> isaco;
    public RecycleViewIsaco(ArrayList<isaco_model> isaco) {
        this.isaco = isaco;

    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView namePiece;
        public TextView nameMark;
        public TextView Price;


        public ViewHolder(View v) {
            super(v);
            namePiece = (TextView) v.findViewById(R.id.name_piece);
            nameMark = (TextView) v.findViewById(R.id.mark_piece);
            Price = (TextView) v.findViewById(R.id.price_piece);

        }
    }



    @Override
    public RecycleViewIsaco.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_piece, parent, false);
        return new RecycleViewIsaco.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecycleViewIsaco.ViewHolder holder, int position) {
        String m = (isaco.get(position)).getPrice();
//        DecimalFormat formatter = new DecimalFormat("#,###,###");
//        m = formatter.format(Integer.parseInt(m));

        holder.namePiece.setText(("نام قطعه :" + isaco.get(position).getpartDesc()));
        holder.nameMark.setText(("مارک :" +isaco.get(position).getmark()));
        holder.Price.setText("قیمت :"+m);

    }

    @Override
    public int getItemCount() {
        return isaco.size();
    }


}
