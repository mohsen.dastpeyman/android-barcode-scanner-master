package com.ariacloud.yadaki;

import java.util.List;

import retrofit.Call;
import retrofit.http.Body;
import retrofit.http.POST;
import retrofit.http.Path;

/**
 * Created by mohsen on 6/28/17.
 */

public interface IsacoService {
    @POST("isaco/search/")
    Call<List<isaco_model>> postIsaco(@Body Data data);
}
